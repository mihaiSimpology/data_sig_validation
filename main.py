import json

import boto3
import botocore
import logging

from datetime import date

S3 = boto3.resource("s3")
logger = logging.getLogger()
logger.setLevel(logging.INFO)

def make_log(event):
    logger.info(event)

def download_data(bucket, key):
    logger.info("download_data function has been called with the following parameters:" + bucket + " / " + key)
    path = "/tmp/" + key
    res = S3.Bucket(bucket).download_file(key, path)
    return res

def put_s3_data(data, bucket, key):
    logger.info("put_s3_data function called with bucket and key:" + bucket + "/" + key)
    obj = S3.Object(bucket, key)
    return obj.put(Body=data)

def get_s3_data(bucket: str, key: str):
    logger.info("get_s3_data function has been called with the following parameters:" + bucket + " / " + key)
    obj = S3.Object(bucket, key)
    return obj.get()['Body'].read().decode('utf-8')

def http_response(statusCode, body=""):
    
    logger.info("Response sent from lambda with status code:" + str(statusCode))
    event = {"statusCode":statusCode, "isBase64Encoded" : False, "body": body, "headers": {}}

    return event

def check_intersection(p1, p2, s1, s2, epsilon):
    
    if (p1[0] - p2[0]) == 0:
        a = 0
    else:
        a = (p1[1] - p2[1])/(p1[0] - p2[0])

    b = p1[1] - a * p1[0]

    if s1[0] < s2[0]:
        dom_x1 , dom_x2 = s1[0],s2[0]
    else:
        dom_x1 , dom_x2 = s2[0],s1[0]

    if s1[1] < s2[1]:
        cod_y1 , cod_y2 = s1[1],s2[1]
    else:
        cod_y1 , cod_y2 = s2[1],s1[1]

    dom = [i for i in range(dom_x1, dom_x2+1, epsilon)]

    for x in dom:
        if cod_y1 < a*x + b and a*x + b < cod_y2:
            return True

    return False

def retrieve_date(doc, anchor):

    position = None
    found = False

    for p in doc["fullTextAnnotation"]["pages"]:
        for b in p["blocks"]:
            for paragraph in b["paragraphs"]:
                words = []
                for word in paragraph["words"]:
                    words.append(''.join([s['text'] for s in  word["symbols"]]))
                words = " ".join(words)
                if words == anchor:
                    position = paragraph["boundingBox"]["vertices"]
                    found = True
            if found:
                break
        if found:
            break
    
    if not found:
        return False, False, False
    
    line_int1 = (position[0]['x'],position[0]['y'])
    line_int2 = (position[1]['x'],position[1]['y'])

    for p in doc["fullTextAnnotation"]["pages"]:
        for b in p["blocks"]:
            for paragraph in b["paragraphs"]:
                pos = paragraph["boundingBox"]["vertices"]
                line_pos1 = (pos[0]['x'],pos[0]['y'])
                line_pos2 = (pos[3]['x'],pos[3]['y'])

                if check_intersection(line_int1,line_int2, line_pos1, line_pos2, 1) and line_int2[0] < line_pos2[0]:
                    words = []
                    for word in paragraph["words"]:
                        words.append(''.join([s['text'] for s in  word["symbols"]]))
                    words = "".join(words)
                    date = words

    try:
        year = int(date[-4:])
        date = date[:-5]
        month = int(date[-2:])
        date = date[:-3]
        day = int(date)
    except:
        year, month, day = False, False, False

    return year, month, day


def handler(event,context):

    src_bucket = event["bucket_sample"]
    src_key = event["key_sample"]

    current_date = event["current_date"]
    submission_date = event["submission_date"]

    error_missing_date = event["error_missing_date"]
    error_validation_date = event["error_validation_date"]

    doc = get_s3_data(src_bucket, src_key)
    doc = json.loads(doc)

    anchors = event["anchors"]

    response = {anchor:{} for anchor in anchors}

    for anchor in anchors:

        year, month, day = retrieve_date(doc, anchor)

        if all(not v for v in [year,month,day]):
            return http_response(400, error_missing_date)

        if month > 12 or month == 0:
            return http_response(400, error_validation_date)
        if day > 31 or day == 0:
            return http_response(400, error_validation_date)

        try:
            current_date = [int(c) for c in current_date.split("/")]
            submission_date = [int(c) for c in submission_date.split("/")]
        except:
            return http_response(400, "Incorrect date format in document!")

        current_date = date(current_date[2], current_date[1], current_date[0])
        submission_date = date(submission_date[2], submission_date[1], submission_date[0])

        doc_date = date(year, month, day)
        delta_sub = doc_date - submission_date
        delta_cur = doc_date - current_date

        if delta_sub.days > 90 and delta_cur.days < 0:
            return http_response(400, error_validation_date)
        
        response[anchor] = "OK"

    return http_response(200, response)