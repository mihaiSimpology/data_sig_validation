import json
from main import handler

def main(event):
    return handler(event, None)

if __name__ == "__main__":
    
    s = {"bucket_sample":"google-ocr-destination",
    "key_sample":"test_image.json",
    "sign_id":"", #optional
    "date_id":"", #optional
    "current_date":"10/10/2006", #upload time date
    "submission_date":"10/8/2006", # 90 days rule,
    "error_missing_signature":"cdcd",
    "error_missing_date":"sdcasdcac",
    "error_validation_date":"cscscsc", 
    "anchors":["Date completed:"]
    }

    response = main(s)

    print(response)