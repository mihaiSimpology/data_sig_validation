zip data_valid.zip main.py 
aws s3 cp data_valid.zip s3://ml-dev.simpology2
aws lambda update-function-code --function-name "DataValidService" --region "eu-central-1" --s3-bucket "ml-dev.simpology2" --s3-key "data_valid.zip"
aws lambda invoke --function-name "DataValidService"  --payload '{"bucket_sample":"google-ocr-destination", "key_sample":"test_image.json", "current_date":"10/10/2006","submission_date":"5/10/2006","error_missing_date":"error_missing_date","error_validation_date":"error_validation_date", "anchors":["Date completed:"]}' here --log-type Tail --query 'LogResult' --output text |  base64 -d
rm data_valid.zip
cat here
